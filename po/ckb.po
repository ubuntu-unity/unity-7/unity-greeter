# Kurdish (Sorani) translation for unity-greeter
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the unity-greeter package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: unity-greeter\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-05-04 04:44+0000\n"
"PO-Revision-Date: 2013-02-22 11:36+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Kurdish (Sorani) <ckb@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-05-05 05:59+0000\n"
"X-Generator: Launchpad (build fbdff7602bd10fb883bf7e2ddcc7fd5a16f60398)\n"

#: ../src/greeter-list.vala:302
#, c-format
msgid "Enter password for %s"
msgstr "ووشەی تێپەڕبوون بنووسە بۆ %s ."

#: ../src/greeter-list.vala:807 ../src/user-list.vala:701
msgid "Password:"
msgstr "تێپەڕەوشە:"

#: ../src/greeter-list.vala:809 ../src/user-list.vala:695
msgid "Username:"
msgstr "ناوی بەکارهێنەر:"

#: ../src/greeter-list.vala:864
msgid "Invalid password, please try again"
msgstr "ووشەی تێپەڕبوون هەڵەیە ،تکایە هەوڵ بدەرەوە"

#: ../src/greeter-list.vala:875
msgid "Failed to authenticate"
msgstr "نەتوانرا ڕێگەت پێبدرێت"

#: ../src/greeter-list.vala:921
msgid "Failed to start session"
msgstr "نەتوانرا دانیشتن پێبکرێت"

#: ../src/greeter-list.vala:936
msgid "Logging in…"
msgstr "دەچیتە ژوورەوە"

#: ../src/main-window.vala:49
msgid "Login Screen"
msgstr "پەردەی چوونەژورەوە"

#: ../src/main-window.vala:118
msgid "Back"
msgstr "بڕۆدواوە"

#: ../src/menubar.vala:220
msgid "Onscreen keyboard"
msgstr "تەختەکلیلی سەر ڕووپەر"

#: ../src/menubar.vala:225
msgid "High Contrast"
msgstr "جیاوازی بەرز"

#: ../src/menubar.vala:231
msgid "Screen Reader"
msgstr "خوێنەرەوەی ڕووپەر"

#: ../src/prompt-box.vala:215
msgid "Session Options"
msgstr "هەڵبژاردنەکانی دانیشتن"

#: ../src/session-list.vala:34
msgid "Select desktop environment"
msgstr ""

#: ../src/shutdown-dialog.vala:101
msgid "Goodbye. Would you like to…"
msgstr "خوات لەگەڵ. دەتەوێت ..."

#: ../src/shutdown-dialog.vala:105 ../src/shutdown-dialog.vala:207
msgid "Shut Down"
msgstr "کوژاندنەوە"

#: ../src/shutdown-dialog.vala:112
msgid "Are you sure you want to shut down the computer?"
msgstr ""

#: ../src/shutdown-dialog.vala:137
msgid ""
"Other users are currently logged in to this computer, shutting down now will "
"also close these other sessions."
msgstr ""

#: ../src/shutdown-dialog.vala:155
msgid "Suspend"
msgstr "ڕاگرتن"

#: ../src/shutdown-dialog.vala:172
msgid "Hibernate"
msgstr "دۆخەپشوو"

#: ../src/shutdown-dialog.vala:190
msgid "Restart"
msgstr "دووبارە پێکردنەوە"

#. Translators: %s is a session name like KDE or Ubuntu
#: ../src/toggle-box.vala:98
#, c-format
msgid "%s (Default)"
msgstr "%s (بنچینەیی)"

#. Help string for command line --version flag
#: ../src/unity-greeter.vala:572
msgid "Show release version"
msgstr "وەشانی بڵاوکراوە پیشان بدە"

#. Help string for command line --test-mode flag
#: ../src/unity-greeter.vala:575
msgid "Run in test mode"
msgstr "پێکردن لە دۆخی تاقیکاری"

#. Arguments and description for --help text
#: ../src/unity-greeter.vala:581
msgid "- Unity Greeter"
msgstr ""

#. Text printed out when an unknown command-line argument provided
#: ../src/unity-greeter.vala:592
#, c-format
msgid "Run '%s --help' to see a full list of available command line options."
msgstr ""

#: ../src/user-list.vala:45
msgid "Guest Session"
msgstr "دانیشتنی میوان"

#: ../src/user-list.vala:438
msgid "Please enter a complete e-mail address"
msgstr ""

#: ../src/user-list.vala:518
msgid "Incorrect e-mail address or password"
msgstr ""

#: ../src/user-list.vala:549
msgid ""
"If you have an account on an RDP or Citrix server, Remote Login lets you run "
"applications from that server."
msgstr ""

#. For 12.10 we still don't support Citrix
#: ../src/user-list.vala:551
msgid ""
"If you have an account on an RDP server, Remote Login lets you run "
"applications from that server."
msgstr ""

#: ../src/user-list.vala:554
msgid "Cancel"
msgstr "پاشگەزبوونەوە"

#: ../src/user-list.vala:555
msgid "Set Up…"
msgstr "ڕێکخستن..."

#: ../src/user-list.vala:557
msgid ""
"You need an Ubuntu Remote Login account to use this service. Would you like "
"to set up an account now?"
msgstr ""

#: ../src/user-list.vala:561
msgid "OK"
msgstr "باشە"

#: ../src/user-list.vala:562
msgid ""
"You need an Ubuntu Remote Login account to use this service. Visit "
"uccs.canonical.com to set up an account."
msgstr ""

#: ../src/user-list.vala:679
msgid "Server type not supported."
msgstr ""

#: ../src/user-list.vala:720
msgid "Domain:"
msgstr ""

#: ../src/user-list.vala:781
msgid "Email address:"
msgstr ""

#. 'Log In' here is the button for logging in.
#: ../src/user-list.vala:825
msgid "Log In"
msgstr ""

#: ../src/user-list.vala:826
#, c-format
msgid "Login as %s"
msgstr ""

#: ../src/user-list.vala:830
msgid "Retry"
msgstr "هەوڵ بدەرەوە"

#: ../src/user-list.vala:831
#, c-format
msgid "Retry as %s"
msgstr ""

#: ../src/user-list.vala:869
msgid "Login"
msgstr "چوونەژوورەوە"

#~ msgid "Logging in..."
#~ msgstr "چوونەناوەوە..."
